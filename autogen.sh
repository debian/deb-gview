#!/bin/sh -x
set -e
for macro in codeset.m4 gettext.m4 glibc21.m4 iconv.m4 lcmessage.m4 progtest.m4; do
	if [ ! -L m4/$macro ] && [ -f m4/$macro ]; then
		rm m4/$macro
	fi
	if [ ! -L m4/$macro ] && [ -f /usr/share/aclocal/$macro ]; then
		(cd m4/; ln -s /usr/share/aclocal/$macro .)
	fi
done
glib-gettextize -f
intltoolize --automake -f
for file in intltool-merge.in intltool-extract.in intltool-update.in; do
	if [ -L $file ] || [ -f $file ]; then
		rm -f $file
	fi
done

autoreconf -ifs
echo "Now you can run ./configure --enable-error-on-warning --enable-compile-werror --enable-compile-warnings"
