<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<refentry id="debgview1">
  <refentryinfo>
    <productname>DEB-GVIEW</productname>
    <productnumber>1</productnumber>
    <date>22 April 2008</date>
  </refentryinfo>
  <refmeta>
    <refentrytitle>DEB-GVIEW</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class="manual">@PACKAGE@ manual</refmiscinfo>
    <refmiscinfo class="version">@VERSION@</refmiscinfo>
  </refmeta>
  <refnamediv id="name">
    <refname>@PACKAGE@</refname>
    <refpurpose>GNOME viewer for .deb package files and contents</refpurpose>
  </refnamediv>
  <refsect1 id="description">
    <title>DESCRIPTION</title>
    <para><emphasis remap="I">@PACKAGE@</emphasis>
    displays Debian control information, devscript details and details of
    the files that would be installed (names, sizes and locations). Files
    within the package can be viewed within the package or externally.
    </para>
    <para><emphasis remap="I">@PACKAGE@</emphasis>
    accepts package files on the command line to support
    the 'open' command in various file managers, one window for each
    package. Individual views can be closed independently or simultaneously
    using 'Quit'.
    </para>
    <para>Packages do not need to be installed to be viewed. Packages do not
    need to be suitable for installation, e.g. a foreign architecture.
    </para>
    <para>Package files and <filename>.changes</filename> files can be viewed
    from local and remote filesystems.</para>
    <para>When loading <filename>.changes</filename> files,
    <emphasis>@PACKAGE@</emphasis> will load each referenced package in a
    new window. When opening a <filename>.changes</filename> file from the
    <emphasis>@PACKAGE@</emphasis> toolbar, the original window is not changed.
    </para>
    <para>When opening Debian packages files from the
    <emphasis>@PACKAGE@</emphasis> toolbar, the package data will replace
    the current window data.
    </para>
    <refsect2 id="usage">
      <title>Usage:</title>
      <para>@PACKAGE@ [<emphasis remap="I">OPTIONS</emphasis>]
        [<emphasis remap="I">file</emphasis>...]</para>
    </refsect2>
    <refsect2 id="command">
      <title>COMMAND:</title>
      <variablelist remap="TP">
        <varlistentry>
          <term>
            <emphasis remap="B">file</emphasis>
          </term>
          <listitem>
            <para>One or more Debian package files (<filename>.deb</filename>)
            to preload or a <filename>.changes</filename> file referencing
            various package files.
            </para>
            <para>Individual package files or a set of packages referenced
            in a <filename>.changes</filename> file can be viewed from local
            and remote filesystems.
            </para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsect2>
    <refsect2 id="gui_options">
      <title>GUI Options:</title>
      <para>See also <filename>gtk-options</filename> (7).</para>
      <variablelist remap="TP">
        <varlistentry>
          <term><option>--display</option>=<emphasis remap="I">DISPLAY</emphasis></term>
          <listitem>
            <para>X display to use</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsect2>
    <refsect2 id="help_options">
      <title>Help Options:</title>
      <variablelist remap="TP">
        <varlistentry>
          <term>-h, <option>--help</option></term>
          <listitem>
            <para>Show help options</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>-v, <option>--version</option></term>
          <listitem>
            <para>Show version number</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsect2>
  </refsect1>
  <refsect1 id="file_previews">
    <title>FILE PREVIEWS</title>
    <para><emphasis remap="I">@PACKAGE@</emphasis> has migrated to the
    XDG specification for config files and uses a dot-file in your
     XDG config directory (default: 
     <filename>~/.config/deb-gview/preview</filename>) to specify
    which applications should be used to preview files from the package.
    You are free to edit this file, @PACKAGE@ will not modify it, although
    the defaults will be replaced if the file is deleted.</para>
    <para>Any previous config in <filename>~/.deb-gview/preview</filename>
    will be moved into the new file but the <filename>~/.deb-gview</filename>
    directory will be preserved - feel free to delete it once any other
    files are moved to the new location.
    </para>
    <para>The first matching pattern will be used, so put the most general
    wildcards (like *) at the end of this file.</para>
    <refsect2 id="groups">
      <title>GROUPS</title>
      <para>The first group contains two
      <emphasis remap="I">@PACKAGE@</emphasis> default settings. 'terminal'
      determines which application to use for viewers that need a terminal.
      You may want to change this to
      <emphasis remap="I">gnome-terminal</emphasis> or
      <emphasis remap="I">konsole</emphasis> from the rather
      limited <emphasis remap="I">xterm</emphasis> default. 'execute_command'
      specifies the command to pass to the terminal application to load an
      external command within the terminal. Most terminal emulators
      support '-e' for this command.
      </para>
      <para>Subsequent group names need to be unique but have no other meaning
      to <emphasis remap="I">@PACKAGE@</emphasis>.
      </para>
    </refsect2>
    <refsect2 id="keys">
      <title>KEYS</title>
      <para><emphasis remap="B">pattern</emphasis>: glob-style pattern to
      match the location of the file in the package (not the filesystem).
      </para>
      <para><emphasis remap="B">viewer</emphasis>: the program to call to
      view the file content (which must exist in your system PATH).
      </para>
      <para><emphasis remap="B">use_terminal</emphasis>: whether to start the
      viewer in a terminal, TRUE or FALSE.
      </para>
      <para><emphasis remap="B">binary_file</emphasis>: whether the viewer
      requires the content as a binary file, TRUE or FALSE. Typically,
      image previews require a binary file.
      </para>
      <para><emphasis remap="B">file_suffix</emphasis>: if the viewer needs
      a specific suffix, specify it here. Note that
      <emphasis remap="I">@PACKAGE@</emphasis> does not insist that the
      suffix of the temporary file matches the suffix of the file within the
      package, it is up to you to ensure that the suffix is suitable for the
      viewer you want to use. Leave blank for no suffix.</para>
    </refsect2>
    <refsect2 id="defaults">
      <title>DEFAULT VALUES</title>
      <para>The defaults for file previews are:</para>
      <literallayout remap=".nf">
# Your choice of terminal application.
#
[deb-gview]
terminal=xterm

[manual pages]
pattern=./usr/share/man/man*/*
viewer=man
use_terminal=true
binary_file=false
file_suffix=

[HTML]
pattern=*.html
viewer=sensible-browser
use_terminal=false
binary_file=false
file_suffix=html

[PNG]
pattern=*.png
viewer=qiv
use_terminal=false
binary_file=true
file_suffix=png

[text]
pattern=*
viewer=gedit
use_terminal=false
binary_file=false
file_suffix=

</literallayout>
    </refsect2>
  </refsect1>
  <refsect1 id="author">
    <title>AUTHOR</title>
    <para><emphasis remap="I">@PACKAGE@</emphasis> was written by
    Neil Williams &lt;linux@codehelp.co.uk&gt;.
    </para>
    <para>This manual page was written by Neil
    Williams &lt;linux@codehelp.co.uk&gt;
    </para>
  </refsect1>
  <refsect1 id="reporting_bugs">
    <title>REPORTING BUGS</title>
    <para><emphasis remap="I">@PACKAGE@</emphasis> is a native Debian
    package and bugs should be reported using the reportbug tool. Reports can
    be found on the Debian bug tracking system for @PACKAGE@.
    </para>
    <para>
      <emphasis remap="I">
        <ulink url="http://bugs.debian.org/cgi-bin/pkgreport.cgi">http://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=deb-gview</ulink>
      </emphasis>
    </para>
  </refsect1>
  <refsect1 id="copyright">
    <title>COPYRIGHT</title>
    <para>This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    </para>
    <para>This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.
    </para>
    <para>You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
    </para>
  </refsect1>
</refentry>
